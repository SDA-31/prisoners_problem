use rand::{seq::SliceRandom, thread_rng, Rng};

const MAX_ELEMENTS: u8 = 100;
const MAX_ITERATIONS: u32 = 1000000;
const MAX_ATTEMPTS: u8 = MAX_ELEMENTS / 2;

fn main() {
	fmt_bench("random", perform_bench(random_search));
	fmt_bench("even-odd", perform_bench(even_odd_search));
	fmt_bench("linked", perform_bench(linked_search));
}

fn fmt_bench(method: &str, val: f32) {
	println!("Average {} search success rate: {}%", method, 100.0 - val);
}

fn perform_bench(search_fn: fn(&mut Vec<u8>, u8) -> (u8, u8)) -> f32 {
	let mut vect: Vec<u8> = (1..MAX_ELEMENTS + 1).collect();
	let mut tup: (u8, u8);
	let mut failed_count: u32 = 0;
	let mut aver = 0.0;

	for _ in 0..MAX_ITERATIONS {
		vect.shuffle(&mut thread_rng());

		for j in 1..MAX_ELEMENTS + 1 {
			tup = search_fn(&mut vect, j);

			if tup.1 as u8 > MAX_ATTEMPTS {
				failed_count += 1;
				break;
			}
		}

		aver += failed_count as f32;
		failed_count = 0;
	}

	aver / (MAX_ITERATIONS as f32 / 100.0)
}

fn linked_search(vect: &mut Vec<u8>, numb: u8) -> (u8, u8) {
	let mut attempts: u8 = 0;
	let mut index = numb;

	loop {
		index = vect[(index - 1) as usize];

		attempts += 1;

		if vect[(index - 1) as usize] == numb || attempts > MAX_ATTEMPTS {
			return (index, attempts);
		}
	}
}

fn random_search(vect: &mut Vec<u8>, numb: u8) -> (u8, u8) {
	let mut rng = rand::thread_rng();
	let mut attempts = vec![];
	let mut index: u8;

	loop {
		index = rng.gen_range(0..MAX_ELEMENTS);

		if attempts.contains(&index) {
			continue;
		}

		attempts.push(index);

		if vect[index as usize] == numb || attempts.len() as u8 > MAX_ATTEMPTS {
			return (index, attempts.len() as u8);
		}
	}
}

fn even_odd_search(vect: &mut Vec<u8>, numb: u8) -> (u8, u8) {
	let mut attempts: u8 = 0;
	let mut index: u8 = (numb - 1) % 2;

	loop {
		if vect[index as usize] == numb || attempts > MAX_ATTEMPTS {
			return (index, attempts);
		}

		index += 2;
		if index >= (vect.len() as u8) {
			index = numb % 2;
		}

		attempts += 1;
	}
}
